# Video Compressor 

# REQIREMENTS:
# 1. Moviepy : https://moviepy.readthedocs.io/en/latest/

import moviepy.editor as mp
import argparse
import sys

# USAGE: python3 vcm.py -v --file [video file]

def resolution_choice()-> tuple:
	# Resolution are not limited to this
	# you can specify width and height manually
	res_240p = (426,240)
	res_360p = (640,360)
	res_480p = (854,480)
	res_720p = (1280,720)
	print(' Following are the available options: ')
	print(' a) 240p b) 360p c) 480p d) 720p ')
	user_pref = input(' Enter the resolution: ')
	print(user_pref)
	if user_pref == '240p':
		res = res_240p
	elif user_pref == '360p':
		res = res_360p
	elif user_pref == '480p':
		res = res_480p
	else:
		res = res_720p
	return res
	
def video_compressor(file):
	clip = mp.VideoFileClip(file)
	clip_width = clip.w
	clip_height = clip.h
	print(' Original Video: ')
	print(f' Width: {clip_width} and Height: {clip_height} or {clip_width}x{clip_height}')
	new_res_size = resolution_choice()
	resized_clip = clip.resize(new_res_size)
	# print(resized_clip.h)
	resized_clip_width = resized_clip.w
	resized_clip_height = resized_clip.h
	print(" Resized Video: ")
	print(f' W: {resized_clip_width} and H: {resized_clip_height} or {resized_clip_width}x{resized_clip_height}')
	rename = input(' Rename[Make sure to give the extension]: ')
	resized_clip.write_videofile(rename)
	return

# TODO: Support for audio file	
def audio_compressor():
	pass

def error_message():
	print('Error Ocuured !!')
	
def main():
	parser = argparse.ArgumentParser(description='vcm: video/audio compressor ')
	parser.add_argument('--file',type=str,action='store',help='media file',required=True)
	parser.add_argument('-a','--audio',action='store_true',help='get the audio file')
	parser.add_argument('-v','--video',action='store_true',help='get the video file')
	parser.add_argument('--version', action='version',version='%(prog)s 1.0[2021/12/18]')
	
	args = parser.parse_args()
	# media_file = args.file
	try:
		media_file = args.file
	except:
		error_message()
		sys.exit()
	
	if args.video:
		video_compressor(media_file)
	elif args.audio:
		audio_compressor(media_file)
	else:
		sys.exit()	
	#vid_file = 'test.mp4' 
	#req_size = 360

#start program
main()
